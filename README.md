# README #

### What is this repository for? ###

* Asset Protection Tool (https://acalvio.atlassian.net/wiki/display/ENG/Sensor+Assets+Protection)
* 1.0
*

### Compilation and testing ###

* To build the project:
*    make
*    make ARM64 (to make the Odroid version)
*
* For unit testing: 
*   aptool -s <directory to encrypt>
*   aptool -s -d <directory to decrypt>

### Contribution guidelines ###

* Code review

### Who do I talk to? ###

* Questions? ethioux@acalvio.com
* or guru@acalvio.com