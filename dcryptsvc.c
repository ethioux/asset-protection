#include <stdio.h>
#include <mcrypt.h>
#include <mhash.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <time.h>
#include <search.h>
#include "net.h"

#define DEFAULT_DIRECTORY "/opt/acalvioe"
#define RAMDISK "/mnt/acalvio"
#define META_DATA_FILE ".ameta"

#define ENCRYPT 0x01
#define DECRYPT 0x02

#define MAX_INDEX 191

#define MAX_SERVER_HALF_KEY_SIZE 256

char eData[256] = {0};

// This is the handle for the metadata file
// It contains the name of the file, its size and its IV
//
int fd_metadata = 0;

typedef struct _METADATA {
    int     cclen;                  // Length of the metadata
    int     iFileNameLength;        // Length of the file name (ASCIIZ)
    char*   szFileName;             // Name of the file (relative path)
    int     iFileSize;              // size of the file
    int     blocksize;              // block size
    char*   IV;                     // IV for the file
} METADATA, *LPMETADATA;

char* encryption_path = NULL;
char* decryption_path = NULL;
char* server_data = NULL;

int AddEntryToMetaDataFile(const char* filename, const int size, const char* IV, const int blocksize) {
    // remove the source directory to make it a relative path
    //
    if (NULL == filename || NULL == IV)
        return -1;

    char* relative_filename = (char*) filename + strlen(encryption_path);
    int cclen = sizeof(int) +           // cclen
                sizeof(int) +           // iFileNameLength
                strlen(relative_filename) + 1 +  // szFileName
                sizeof(int) +           // iFileSize
                sizeof(int) +           // blocksize
                blocksize;

    int filenamesize = strlen(relative_filename) + 1;
    write(fd_metadata, &cclen, sizeof(int));
    write(fd_metadata, &filenamesize, sizeof(int));
    write(fd_metadata, relative_filename, filenamesize);
    write(fd_metadata, &size, sizeof(int));
    write(fd_metadata, &blocksize, sizeof(int));
    write(fd_metadata, IV, blocksize);

    return 0;
}

int ReadMetaDataFile() {
    int bytesread = 0;
    while ( 1 ) {
        LPMETADATA data = (LPMETADATA) malloc(sizeof(METADATA));
        // read data->cclen
        bytesread = read(fd_metadata, &data->cclen, sizeof(int));
        if (bytesread == 0)
            break;

        // read data->iFileNameLength
        bytesread = read(fd_metadata, &data->iFileNameLength, sizeof(int));
        if (data->iFileNameLength > 0) {
            data->szFileName = malloc(data->iFileNameLength + strlen(decryption_path));
            strcpy(data->szFileName, decryption_path);
            bytesread = read(fd_metadata, &data->szFileName[strlen(decryption_path)], data->iFileNameLength);
        }

        // read data->iFileSize
        bytesread = read(fd_metadata, &data->iFileSize, sizeof(int));
        if (bytesread == 0)
            break;

        // read data->blocksize
        bytesread = read(fd_metadata, &data->blocksize, sizeof(int));
        if (bytesread == 0)
            break;

        if (data->blocksize > 0) {
            data->IV = malloc(data->blocksize);
            bytesread = read(fd_metadata, data->IV, data->blocksize);
        }

        ENTRY e = {0};
        ENTRY *ep = NULL;
        e.key = data->szFileName;
        e.data = data;
        ep = hsearch(e, ENTER);
    }

    return bytesread;
}

char* EncryptFile(const char* filename, int* filesize, int* blocksize_ret) {
    MCRYPT td;
    int keysize=16; /* 128 bits */
    char* password = eData;
    char* key = calloc(1, keysize);
    char* destpath = malloc(strlen(filename) + 5);
    char* IV;
    int init = 0;
    int i = 0;
    char* buffer = NULL;
    int blocksize = 0;
    KEYGEN data;

    sprintf(destpath, "%s.enc", filename);
    printf("Encrypting %s\n", filename);
    *filesize = 0;

    struct stat FileAttrib;
    stat(filename, &FileAttrib);
    int fd_source = open(filename, O_RDONLY);
    int fd_destination = open(destpath, O_RDWR | O_CREAT | O_TRUNC, FileAttrib.st_mode);

    if (fd_source >= 0 && fd_destination >= 0) {
        td = mcrypt_module_open("serpent", NULL, "cbc", NULL);
        if (td == MCRYPT_FAILED) {
            printf("Encryption library failed to open.\n");
            return NULL;
        }

        data.hash_algorithm[0] = MHASH_MD5;
        data.count = 0;
        data.salt = NULL;
        data.salt_size = 0;
        mhash_keygen_ext( KEYGEN_MCRYPT, data, (void*) key, keysize, password, strlen(password));
        //memmove( key, password, strlen(password));
        IV = malloc(mcrypt_enc_get_iv_size(td));
        srand(time(0));
        for (i=0; i< mcrypt_enc_get_iv_size( td); i++) {
            IV[i]=rand();
        }

        init=mcrypt_generic_init( td, key, keysize, IV);
        if (init<0) {
            mcrypt_perror(init);
            return NULL;
        }

        blocksize = mcrypt_enc_get_block_size(td);
        *blocksize_ret = blocksize;
        buffer = malloc(blocksize);

        while ( 1 ) {
            int readsize = 0;
            memset(buffer, 0, blocksize);

            if ((readsize = read (fd_source, buffer, blocksize)) > 0 ) {
                *filesize += readsize;
                mcrypt_generic (td, buffer, blocksize);
                write(fd_destination, buffer, blocksize);
            }
            else break;
        }

        mcrypt_generic_end(td);

        free(buffer);
        memset(key, 0, keysize);
        close(fd_source);
        close(fd_destination);
    }

    free(destpath);

    return IV;
}

int DecryptFile(const char* filename, char* IV, int filesize) {
    MCRYPT td;
    int keysize=16; /* 128 bits */
    char* password = eData;
    char* key = calloc(1, keysize);
    char* destpath = malloc(strlen(filename) + 5);
    int init = 0;
    int i = 0;
    char* buffer = NULL;
    int blocksize = 0;
    KEYGEN data;

    sprintf(destpath, "%s.dec", filename);
    printf("Decrypting %s\n", filename);

    struct stat FileAttrib;
    stat(filename, &FileAttrib);
    int fd_source = open(filename, O_RDONLY);
    int fd_destination = open(destpath, O_RDWR | O_CREAT, FileAttrib.st_mode);

    if (fd_source >= 0 && fd_destination >= 0) {
        td = mcrypt_module_open("serpent", NULL, "cbc", NULL);
        if (td == MCRYPT_FAILED) {
            printf("Encryption library failed to open.\n");
            return 1;
        }

        data.hash_algorithm[0] = MHASH_MD5;
        data.count = 0;
        data.salt = NULL;
        data.salt_size = 0;
        mhash_keygen_ext( KEYGEN_MCRYPT, data, (void*) key, keysize, password, strlen(password));
        //memmove( key, password, strlen(password));

        init=mcrypt_generic_init( td, key, keysize, IV);
        if (init<0) {
            mcrypt_perror(init);
            return 1;
        }

        blocksize = mcrypt_enc_get_block_size(td);
        buffer = malloc(blocksize);

        while ( 1 ) {
            int readsize = 0;
            memset(buffer, 0, blocksize);

            if ((readsize = read (fd_source, buffer, blocksize)) > 0 ) {
                mdecrypt_generic (td, buffer, blocksize);
                if (blocksize > filesize)
                    blocksize = filesize;

                write(fd_destination, buffer, blocksize);
                filesize -= readsize;
            }
            else break;
        }

        mcrypt_generic_end(td);

        free(buffer);
        memset(key, 0, keysize);
        close(fd_source);
        close(fd_destination);
    }

    free(destpath);
    return 0;
}

// This is to get the encryption passphrase
// The name of the function was chosen to not make it obvious
//
void GetTentativeBits() {
    unsigned char bits[6];
    char hexbit[5];

    // Get the MAC Address
    //
    getMACAddress(bits);

    // Sanitize the eData, which is the passphrase
    //
    memset(eData, 0, 128);

    for (int i = 0; i < 6; i++) {
        sprintf(hexbit, "%02X", bits[i]);
        strcat(eData, hexbit);
        if (i < 5)
            strcat(eData, ":");
    }

    if (server_data != NULL)
        strcat(eData, server_data);
    else {
        server_data = malloc(MAX_SERVER_HALF_KEY_SIZE);
        if (NULL != server_data) {
            QueryServer(eData, server_data, MAX_SERVER_HALF_KEY_SIZE);
            strcat(eData, server_data);
        }
    }
}

void ScanDirectoryAndEncrypt(const char* directory) {
    DIR *d;
    struct dirent *dir;

    printf("Processing %s\n", directory);
    d = opendir(directory);
    GetTentativeBits();

    // If the directory can be opened, we can enumerate the files
    //
    if (d)
    {
        while ((dir = readdir(d)) != NULL) {
            if (dir->d_type == DT_DIR) {
                if (strcmp(dir->d_name, ".") == 0 ||
                    strcmp(dir->d_name, "..") == 0) {
                      // Don't process the . or .. directories
                }
                else {
                    char* newdir = malloc(strlen(directory) + strlen(dir->d_name) + 2);
                    sprintf(newdir, "%s/%s", directory, dir->d_name);
                    ScanDirectoryAndEncrypt(newdir);
                    free(newdir);
                }
            }
            else {
                if (dir->d_type == DT_REG) {
                    char* filepath = malloc(strlen(directory) + strlen(dir->d_name) + 16);
                    char* enc_filepath = malloc(strlen(directory) + strlen(dir->d_name) + 16);
                    sprintf(filepath, "%s/%s", directory, dir->d_name);
                    int filesize = 0;
                    int blocksize = 0;
                    char* IV = EncryptFile(filepath, &filesize, &blocksize);
                    AddEntryToMetaDataFile(filepath, filesize, IV, blocksize);
                    unlink(filepath);
                    sprintf(enc_filepath, "%s.enc", filepath);
                    rename(enc_filepath, filepath);
                    free(filepath);
                }
            }
        }

        closedir(d);
    }
}

void ScanDirectoryAndDecrypt(const char* directory) {
    DIR *d;
    struct dirent *dir;

    printf("Processing %s\n", directory);
    d = opendir(directory);
    GetTentativeBits();

    // If the directory can be opened, we can enumerate the files
    //
    if (d)
    {
        while ((dir = readdir(d)) != NULL) {
            if (dir->d_type == DT_DIR) {
                if (strcmp(dir->d_name, ".") == 0 ||
                    strcmp(dir->d_name, "..") == 0) {
                      // Don't process the . or .. directories
                }
                else {
                    char* newdir = malloc(strlen(directory) + strlen(dir->d_name) + 2);
                    sprintf(newdir, "%s/%s", directory, dir->d_name);
                    ScanDirectoryAndDecrypt(newdir);
                    free(newdir);
                }
            }
            else {
                if (dir->d_type == DT_REG) {
                    char* filepath = malloc(strlen(directory) + strlen(dir->d_name) + 16);
                    char* dec_filepath = malloc(strlen(directory) + strlen(dir->d_name) + 16);
                    sprintf(filepath, "%s/%s", directory, dir->d_name);
                    int filesize = 0;
                    int blocksize = 0;
                    ENTRY e = {0};
                    e.key = filepath;
                    ENTRY* ep = hsearch(e, FIND);
                    if (ep != NULL) {
                        LPMETADATA data = (LPMETADATA) ep->data;
                        DecryptFile(filepath, data->IV, data->iFileSize);
                        unlink(filepath);
                        sprintf(dec_filepath, "%s.dec", filepath);
                        rename(dec_filepath, filepath);
                    }

                    free(filepath);
                }
            }
        }

        closedir(d);
    }
}

/**
  * Create the metadata file and sets its size to zero
  * if it already exist
  */
int CreateMetaDataFile() {
    mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
    fd_metadata = open(META_DATA_FILE, O_RDWR | O_CREAT | O_TRUNC, mode);
    return fd_metadata;
}

/**
  * open the metadata file
  */
int OpenMetaDataFile() {
    fd_metadata = open(META_DATA_FILE, O_RDONLY);
    return fd_metadata;
}

int CreateMetaDataTable() {
    hcreate(MAX_INDEX);
}

int main(char argc, char** argv) {
    int operation = ENCRYPT;
    int i = 0;

    for (i = 0; i < argc; i++) {
        if (strcmp(argv[i], "-x") == 0)
            operation = DECRYPT;
        if (strcmp(argv[i], "-s") == 0)
            encryption_path = strdup(argv[i+1]);
        if (strcmp(argv[i], "-d") == 0)
            decryption_path = strdup(argv[i+1]);
    }

    if (operation == ENCRYPT) {
        printf("Encrypting the source directory\n");
        if (encryption_path == NULL)
            encryption_path = strdup(DEFAULT_DIRECTORY);
        if (CreateMetaDataFile() >= 0)
            ScanDirectoryAndEncrypt(encryption_path);
        else
            printf("Unable to create the metadata file, aborted.");
    }

    if (operation == DECRYPT) {
        if (decryption_path == NULL)
            decryption_path = strdup(RAMDISK);
        if (OpenMetaDataFile() >= 0) {
            CreateMetaDataTable();
            ReadMetaDataFile();
            ScanDirectoryAndDecrypt(decryption_path);
        }
    }

    free(encryption_path);
    free(decryption_path);
    close(fd_metadata);
}
