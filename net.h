#ifndef _NET_C_
#define _NET_C_

int getMACAddress(unsigned char mac_address[6]);
int QueryServer(const char* ID, char* data, int cclen);

#endif
