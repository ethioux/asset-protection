CC=gcc
CFLAGS=-I. -l mcrypt -l mhash
DEPS = net.h
OBJ = dcryptsvc.o net.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

aptool: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)

arm64: 
	echo Building the ARM64 version
	$(MAKE) CC=aarch64-linux-gnu-gcc-5 aptool 

clean:
	rm *.o
	rm aptool
